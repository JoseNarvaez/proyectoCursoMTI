/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control.Modulo;

import Modelo.Modulo.BeanModulo;
import Modelo.Modulo.DaoModulo;
import Modelo.Turbogenerador.BeanTurbogenerador;
import javax.swing.JOptionPane;

/**
 *
 * @author NarvaezLapCDS
 */
public class ControlModulo {

    public boolean registrarModulo(String descripcion, double precio, BeanTurbogenerador turbo) {
        BeanModulo modulo = new BeanModulo();
        modulo.setDescripcion(descripcion);
        modulo.setPrecio(precio);
        modulo.setIdTurbogenerador(turbo.getId());
        return new DaoModulo().registrarModuloTurbo(modulo);
    }

    public boolean modificarModulo(String descripcion, double precio, BeanTurbogenerador turbo, int id) {
        BeanModulo modulo = new BeanModulo();
        modulo.setIdModulo(id);
        modulo.setDescripcion(descripcion);
        modulo.setPrecio(precio);
        modulo.setIdTurbogenerador(turbo.getId());
        return new DaoModulo().modificarModuloTurbo(modulo);
    }

}
