/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control.Pieza;

import Modelo.Modulo.BeanModulo;
import Modelo.Pieza.BeanPieza;
import Modelo.Pieza.DaoPieza;
import Modelo.Turbogenerador.BeanTurbogenerador;

/**
 *
 * @author NarvaezLapCDS
 */
public class ControlPieza {

    public boolean registrarPieza(String descripcion, BeanTurbogenerador turbo, BeanModulo modulo, double precio) {
        BeanPieza pieza = new BeanPieza(0, descripcion, modulo.getIdModulo(), turbo.getId(),precio);
        if(turbo.getId()>0){
            return new DaoPieza().registrarPiezaTurbo(pieza);
        }else{
            return new DaoPieza().registrarPiezaModulo(pieza);
        }
    }

    public boolean modificarPieza(String descripcion, BeanTurbogenerador turbo, BeanModulo modulo, double precio,int idPieza) {
        BeanPieza pieza = new BeanPieza(idPieza, descripcion, modulo.getIdModulo(), turbo.getId(),precio);
        if(turbo.getId()>0){
            return new DaoPieza().modificarPiezaTurbo(pieza);
        }else{
            return new DaoPieza().modificarPiezaModulo(pieza);
        }
    }
    
}
