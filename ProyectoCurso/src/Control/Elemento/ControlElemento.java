/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control.Elemento;

import Modelo.Elemento.BeanElemento;
import Modelo.Elemento.DaoElemento;
import Modelo.Pieza.BeanPieza;
import Modelo.Turbogenerador.BeanTurbogenerador;

/**
 *
 * @author NarvaezLapCDS
 */
public class ControlElemento {

    public boolean registrarElemento(String descripcion, BeanTurbogenerador turbo, BeanPieza pieza, double precio) {
        BeanElemento elemento = new BeanElemento(0, descripcion, turbo.getId(), pieza.getIdPieza(),precio);
        if(turbo.getId()>0){
            return new DaoElemento().registrarElementoTurbo(elemento);
        }else{
            return new DaoElemento().registrarElementoPieza(elemento);
        }
    }

    public boolean modificarElemento(String descripcion, BeanTurbogenerador turbo, BeanPieza pieza, double precio, int idEditar) {
        BeanElemento elemento = new BeanElemento(idEditar, descripcion, turbo.getId(), pieza.getIdPieza(),precio);
        if(turbo.getId()>0){
            return new DaoElemento().modificarElementoTurbo(elemento);
        }else{
            return new DaoElemento().modificarElementoModulo(elemento);
        }
    }
    
}
