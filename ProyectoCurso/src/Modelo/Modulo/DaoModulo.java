/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Modulo;

import Modelo.ConexionSQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author NarvaezLapCDS
 */
public class DaoModulo {

    private Connection con;
    private PreparedStatement pstm;
    private ResultSet rs;
    private boolean resultado;

    /**
     *
     * @return
     */
    public DefaultTableModel consultarModulos() {
        String[] titulos = {"ID", "Descripción", "Precio", "Turbogenerador"};
        DefaultTableModel modeloModulo = new DefaultTableModel(null, titulos);
        try {
            con = ConexionSQL.obtenerConexion();
            pstm = con.prepareStatement("select * from tModulo");
            rs = pstm.executeQuery();
            String[] fila = new String[4];
            while (rs.next()) {
                fila[0] = rs.getString("tModuloId");
                fila[1] = rs.getString("tModuloDescripcion");
                fila[2] = rs.getString("tModuloPrecio");
                fila[3] = rs.getString("tTurbogeneradorIdF");
                modeloModulo.addRow(fila);
            }

        } catch (Exception e) {
            System.out.println("Error " + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.out.println("Error " + e);
            }
        }

        return modeloModulo;
    }
    
    public boolean registrarModuloTurbo(BeanModulo modulo) {
        try {
            con = ConexionSQL.obtenerConexion();
            pstm = con.prepareStatement("INSERT INTO tModulo (tModuloDescripcion ,tTurbogeneradorIdF,tModuloPrecio) VALUES (?,?,?)");
            pstm.setString(1, modulo.getDescripcion());
            pstm.setInt(2, modulo.getIdTurbogenerador());
            pstm.setDouble(3, modulo.getPrecio());
            resultado = pstm.executeUpdate()==1;            
        } catch (Exception e) {
            System.out.println("Error registrarModuloTurbo : "+e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.out.println("Error registrarModulo cerrar " + e);
            }
        }
        return resultado;
    }

    public boolean modificarModuloTurbo(BeanModulo modulo) {
        try {
            con = ConexionSQL.obtenerConexion();
            pstm = con.prepareStatement("UPDATE tModulo SET tModuloDescripcion = ? ,"
                    + "tTurbogeneradorIdF = ? ,tModuloPrecio = ? "
                    + "WHERE tModuloId=?");
            pstm.setString(1, modulo.getDescripcion());
            pstm.setInt(2, modulo.getIdTurbogenerador());
            pstm.setDouble(3, modulo.getPrecio());
            pstm.setInt(4, modulo.getIdModulo());
            resultado = pstm.executeUpdate()==1;            
        } catch (Exception e) {
            System.out.println("Error modificarModuloTurbo : "+e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.out.println("Error modificarModuloTurbo cerrar " + e);
            }
        }
        return resultado;
    }

    public boolean modificarModulo(BeanModulo modulo) {
        try {
            con = ConexionSQL.obtenerConexion();
            pstm = con.prepareStatement("UPDATE tModulo SET tModuloDescripcion = ? WHERE tModuloId=?");
            pstm.setString(1, modulo.getDescripcion());
            pstm.setInt(2, modulo.getIdModulo());
            resultado = pstm.executeUpdate()==1;            
        } catch (Exception e) {
            System.out.println("Error modificarModulo : "+e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.out.println("Error modificarModulo cerrar " + e);
            }
        }
        return resultado;
    }

    public List<BeanModulo> consultarListaModulos() {
        List<BeanModulo> turbos = new ArrayList<>();
        try {
            con = ConexionSQL.obtenerConexion();
            pstm = con.prepareStatement("select * from tModulo");
            rs = pstm.executeQuery();
            BeanModulo modelo;
            modelo = new BeanModulo();
            modelo.setDescripcion("Seleccione...");
            turbos.add(modelo);
            while (rs.next()) {
                modelo = new BeanModulo();
                modelo.setIdModulo(Integer.parseInt(rs.getString("tModuloID")));
                modelo.setDescripcion(rs.getString("tModuloDescripcion"));
                modelo.setIdTurbogenerador(rs.getInt("tTurbogeneradorIdF"));
                turbos.add(modelo);
            }
        } catch (SQLException ex) {
            System.out.println("Error " + ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.out.println("Error " + e);
            }

        }
        return turbos;
    }

}
