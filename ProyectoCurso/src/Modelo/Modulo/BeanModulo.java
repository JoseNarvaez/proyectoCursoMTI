/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Modulo;

/**
 *
 * @author NarvaezLapCDS
 */
public class BeanModulo {
    private int idModulo;
    private String descripcion;
    private double precio;
    private int idTurbogenerador;

    public BeanModulo() {
    }

    public BeanModulo(int idModulo, String descripcion, double precio, int idTurbogenerador) {
        this.idModulo = idModulo;
        this.descripcion = descripcion;
        this.precio = precio;
        this.idTurbogenerador = idTurbogenerador;
    }

    public int getIdModulo() {
        return idModulo;
    }

    public void setIdModulo(int idModulo) {
        this.idModulo = idModulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getIdTurbogenerador() {
        return idTurbogenerador;
    }

    public void setIdTurbogenerador(int idTurbogenerador) {
        this.idTurbogenerador = idTurbogenerador;
    }

    @Override
    public String toString() {
        return descripcion;
    }
}
