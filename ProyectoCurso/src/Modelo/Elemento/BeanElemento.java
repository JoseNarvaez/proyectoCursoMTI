/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Elemento;

/**
 *
 * @author NarvaezLapCDS
 */
public class BeanElemento {
    private int idElemento;
    private String decripcion;
    private int idTurbogenerador;
    private int idPieza;
    private double precio;

    public BeanElemento() {
    }

    public BeanElemento(int idElemento, String decripcion, int idTurbogenerador, int idPieza, double precio) {
        this.idElemento = idElemento;
        this.decripcion = decripcion;
        this.idTurbogenerador = idTurbogenerador;
        this.idPieza = idPieza;
        this.precio = precio;
    }

    public int getIdElemento() {
        return idElemento;
    }

    public void setIdElemento(int idElemento) {
        this.idElemento = idElemento;
    }

    public String getDecripcion() {
        return decripcion;
    }

    public void setDecripcion(String decripcion) {
        this.decripcion = decripcion;
    }

    public int getIdTurbogenerador() {
        return idTurbogenerador;
    }

    public void setIdTurbogenerador(int idTurbogenerador) {
        this.idTurbogenerador = idTurbogenerador;
    }

    public int getIdPieza() {
        return idPieza;
    }

    public void setIdPieza(int idPieza) {
        this.idPieza = idPieza;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
    
    
    
}
