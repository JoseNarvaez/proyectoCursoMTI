/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Elemento;

import Modelo.ConexionSQL;
import Modelo.Pieza.BeanPieza;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author NarvaezLapCDS
 */
public class DaoElemento {
    private Connection con;
    private PreparedStatement pstm;
    private ResultSet rs;
    private boolean resultado;
    
    public DefaultTableModel consultarElementos() {
        String[] titulos = {"ID", "Descripción", "Precio", "Pieza", "Turbogenerador"};
        DefaultTableModel modeloModulo = new DefaultTableModel(null, titulos);
        try {
            con = ConexionSQL.obtenerConexion();
            pstm = con.prepareStatement("select * from tElemento");
            rs = pstm.executeQuery();
            String[] fila = new String[5];
            while (rs.next()) {
                fila[0] = rs.getString("tElementoId");
                fila[1] = rs.getString("tElementoDescripcion");
                fila[2] = rs.getString("tElementoPrecio");
                fila[3] = rs.getString("tPiezaIdF");
                fila[4] = rs.getString("tTurbogeneradorIdF");
                modeloModulo.addRow(fila);
            }

        } catch (Exception e) {
            System.out.println("Error consultarElementos " + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.out.println("Error consultarElementos cerrar" + e);
            }
        }

        return modeloModulo;
    }

    public boolean registrarElementoTurbo(BeanElemento elemento) {
        try {
            con = ConexionSQL.obtenerConexion();
            pstm = con.prepareStatement("INSERT INTO tElemento(tElementoDescripcion,tTurbogeneradorIdF,tElementoPrecio) VALUES (?,?,?)");
            pstm.setString(1, elemento.getDecripcion());
            pstm.setInt(2, elemento.getIdTurbogenerador());
            pstm.setDouble(3, elemento.getPrecio());
            resultado = pstm.executeUpdate() == 1;
        } catch (Exception e) {
            System.out.println("Error registrarElementoTurbo " + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
            } catch (Exception e) {
                System.out.println("Error cerrar registrarElementoTurbo " + e);
            }
        }
        return resultado;
    }

    public boolean registrarElementoPieza(BeanElemento elemento) {
        try {
            con = ConexionSQL.obtenerConexion();
            pstm = con.prepareStatement("INSERT INTO tElemento(tElementoDescripcion,tPiezaIdF,tElementoPrecio) VALUES (?,?,?)");
            pstm.setString(1, elemento.getDecripcion());
            pstm.setInt(2, elemento.getIdPieza());
            pstm.setDouble(3, elemento.getPrecio());
            resultado = pstm.executeUpdate() == 1;
        } catch (Exception e) {
            System.out.println("Error registrarElementoPieza " + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
            } catch (Exception e) {
                System.out.println("Error cerrar registrarElementoPieza " + e);
            }
        }
        return resultado;
    }

    public boolean modificarElementoTurbo(BeanElemento elemento) {
        try {
            con = ConexionSQL.obtenerConexion();
            pstm = con.prepareStatement("UPDATE tElemento SET tElementoDescripcion = ? ,tPiezaIdF =null ,tTurbogeneradorIdF = ? ,tElementoPrecio = ? WHERE tElementoId = ?");
            pstm.setString(1, elemento.getDecripcion());
            pstm.setInt(2, elemento.getIdTurbogenerador());
            pstm.setDouble(3, elemento.getPrecio());
            pstm.setInt(4, elemento.getIdElemento());
            resultado = pstm.executeUpdate() == 1;
        } catch (Exception e) {
            System.out.println("Error modificarElementoTurbo " + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
            } catch (Exception e) {
                System.out.println("Error cerrar modificarElementoTurbo " + e);
            }
        }
        return resultado;
    }

    public boolean modificarElementoModulo(BeanElemento elemento) {
        try {
            con = ConexionSQL.obtenerConexion();
            pstm = con.prepareStatement("UPDATE tElemento SET tElementoDescripcion = ? ,tPiezaIdF =? ,tTurbogeneradorIdF = null ,tElementoPrecio = ? WHERE tElementoId = ?");
            pstm.setString(1, elemento.getDecripcion());
            pstm.setInt(2, elemento.getIdPieza());
            pstm.setDouble(3, elemento.getPrecio());
            pstm.setInt(4, elemento.getIdElemento());
            resultado = pstm.executeUpdate() == 1;
        } catch (Exception e) {
            System.out.println("Error modificarElementoModulo " + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
            } catch (Exception e) {
                System.out.println("Error cerrar modificarElementoModulo " + e);
            }
        }
        return resultado;
    }
    
}
