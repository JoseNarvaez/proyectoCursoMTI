/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Turbogenerador;

import Modelo.ConexionSQL;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author NarvaezLapCDS
 */
public class DaoTurbogenerador {

    private Connection con;
    private PreparedStatement pstm;
    private CallableStatement cstm;
    private ResultSet rs;

    public List<BeanTurbogenerador> consultarTurbogeneradores() {
        List<BeanTurbogenerador> turbos = new ArrayList<>();
        try {
            con = ConexionSQL.obtenerConexion();
            cstm = con.prepareCall("{CALL uspConsultarTurbogeerador}");
            rs = cstm.executeQuery();
            BeanTurbogenerador turbo;
            turbo = new BeanTurbogenerador();
            turbo.setDescripcion("Seleccione...");
            turbos.add(turbo);
            while (rs.next()) {
                turbo = new BeanTurbogenerador();
                turbo.setId(Integer.parseInt(rs.getString("tTurbogeneradorID")));
                turbo.setDescripcion(rs.getString("tTurbogeneradorDescripcion"));
                turbos.add(turbo);
            }
        } catch (SQLException ex) {
            System.out.println("Error " + ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (cstm != null) {
                    cstm.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.out.println("Error " + e);
            }

        }
        return turbos;
    }

}
