/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Pieza;

import Modelo.ConexionSQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author NarvaezLapCDS
 */
public class DaoPieza {

    private Connection con;
    private PreparedStatement pstm;
    private ResultSet rs;
    private boolean resultado;

    public DefaultTableModel consultarPiezas() {
        String[] titulos = {"ID", "Descripción", "Precio", "Modulo", "Turbogenerador"};
        DefaultTableModel modeloModulo = new DefaultTableModel(null, titulos);
        try {
            con = ConexionSQL.obtenerConexion();
            pstm = con.prepareStatement("select * from tPieza");
            rs = pstm.executeQuery();
            String[] fila = new String[5];
            while (rs.next()) {
                fila[0] = rs.getString("tPiezaId");
                fila[1] = rs.getString("tPiezaDescripcion");
                fila[2] = rs.getString("tPiezaPrecio");
                fila[3] = rs.getString("tModuloIdF");
                fila[4] = rs.getString("tTurbogeneradorIdF");
                modeloModulo.addRow(fila);
            }

        } catch (Exception e) {
            System.out.println("Error consultarPiezas " + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.out.println("Error consultarPiezas cerrar" + e);
            }
        }

        return modeloModulo;
    }

    public boolean registrarPiezaModulo(BeanPieza pieza) {
        try {
            con = ConexionSQL.obtenerConexion();
            pstm = con.prepareStatement("INSERT INTO tPieza(tPiezaDescripcion,tModuloIdF,tPiezaPrecio) VALUES (?,?,?)");
            pstm.setString(1, pieza.getDescripcion());
            pstm.setInt(2, pieza.getIdModulo());
            pstm.setDouble(3, pieza.getPrecio());
            resultado = pstm.executeUpdate() == 1;
        } catch (Exception e) {
            System.out.println("Error registrarPiezaModulo " + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
            } catch (Exception e) {
                System.out.println("Error cerrar registrarPiezaModulo " + e);
            }
        }
        return resultado;
    }

    public boolean registrarPiezaTurbo(BeanPieza pieza) {
        try {
            con = ConexionSQL.obtenerConexion();
            pstm = con.prepareStatement("INSERT INTO tPieza(tPiezaDescripcion,tTurbogeneradorIdF,tPiezaPrecio) VALUES (?,?,?)");
            pstm.setString(1, pieza.getDescripcion());
            pstm.setInt(2, pieza.getIdTurbo());
            pstm.setDouble(3, pieza.getPrecio());
            resultado = pstm.executeUpdate() == 1;
        } catch (Exception e) {
            System.out.println("Error registrarPiezaTurbo " + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
            } catch (Exception e) {
                System.out.println("Error cerrar registrarPiezaTurbo " + e);
            }
        }
        return resultado;
    }

    public boolean modificarPiezaTurbo(BeanPieza pieza) {
        try {
            con = ConexionSQL.obtenerConexion();
            pstm = con.prepareStatement("UPDATE tPieza SET tPiezaDescripcion = ?,tModuloIdF = null,tTurbogeneradorIdF = ?,tPiezaPrecio = ? WHERE tPiezaId=?");
            pstm.setString(1, pieza.getDescripcion());
            pstm.setInt(2, pieza.getIdTurbo()>0?pieza.getIdTurbo():null);
            pstm.setDouble(3, pieza.getPrecio());
            pstm.setInt(4, pieza.getIdPieza());
            resultado = pstm.executeUpdate() == 1;
        } catch (Exception e) {
            System.out.println("Error modificarPiezaTurbo " + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
            } catch (Exception e) {
                System.out.println("Error cerrar modificarPiezaTurbo " + e);
            }
        }
        return resultado;
    }

    public boolean modificarPiezaModulo(BeanPieza pieza) {
        try {
            con = ConexionSQL.obtenerConexion();
            pstm = con.prepareStatement("UPDATE tPieza SET tPiezaDescripcion = ?,tModuloIdF = ?,tTurbogeneradorIdF = null,tPiezaPrecio = ? WHERE tPiezaId=?");
            pstm.setString(1, pieza.getDescripcion());
            pstm.setInt(2, pieza.getIdModulo());
            pstm.setDouble(3, pieza.getPrecio());
            pstm.setInt(4, pieza.getIdPieza());
            resultado = pstm.executeUpdate() == 1;
        } catch (Exception e) {
            System.out.println("Error modificarPiezaModulo " + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
            } catch (Exception e) {
                System.out.println("Error cerrar modificarPiezaModulo " + e);
            }
        }
        return resultado;
    }

    public List<BeanPieza> consultarListaPiezas() {
        List<BeanPieza> turbos = new ArrayList<>();
        try {
            con = ConexionSQL.obtenerConexion();
            pstm = con.prepareStatement("select * from tPieza");
            rs = pstm.executeQuery();
            BeanPieza turbo;
            turbo = new BeanPieza();
            turbo.setDescripcion("Seleccione...");
            turbos.add(turbo);
            while (rs.next()) {
                turbo = new BeanPieza();
                turbo.setIdPieza(Integer.parseInt(rs.getString("tPiezaId")));
                turbo.setDescripcion(rs.getString("tPiezaDescripcion"));
                turbos.add(turbo);
            }
        } catch (SQLException ex) {
            System.out.println("Error consultarListaPiezas " + ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.out.println("Error consultarListaPiezas cierres " + e);
            }

        }
        return turbos;
    }

}
