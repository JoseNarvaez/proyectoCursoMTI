/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Pieza;

/**
 *
 * @author NarvaezLapCDS
 */
public class BeanPieza {
    
    private int idPieza;
    private String descripcion;
    private int idModulo;
    private int idTurbo;
    private double precio;

    public BeanPieza() {
    }

    public BeanPieza(int idPieza, String descripcion, int idModulo, int idTurbo, double precio) {
        this.idPieza = idPieza;
        this.descripcion = descripcion;
        this.idModulo = idModulo;
        this.idTurbo = idTurbo;
        this.precio = precio;
    }



    public int getIdPieza() {
        return idPieza;
    }

    public void setIdPieza(int idPieza) {
        this.idPieza = idPieza;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdModulo() {
        return idModulo;
    }

    public void setIdModulo(int idModulo) {
        this.idModulo = idModulo;
    }

    public int getIdTurbo() {
        return idTurbo;
    }

    public void setIdTurbo(int idTurbo) {
        this.idTurbo = idTurbo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
    

    @Override
    public String toString() {
        return descripcion;
    }
    
    
}
