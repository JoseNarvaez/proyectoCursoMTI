package Modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que establece el medio de comunicaci�n con la base de datos hecha en
 * Oracle
 */
public class ConexionSQL {

    private static String ipAddress;
    private static String dbName;
    private static String user;
    private static String password;
    private static String service;
    private static ResourceBundle propiedadesBD;

    /**
     * M�todo que carga el driver, establece la conexi�n.
     *
     * @ return Connection
     *
     */
    public static Connection obtenerConexion() {
        
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        
        ipAddress = "localhost";
        dbName = "proyecto";
        user = "sa";
        password = "root";
        service = "1433";
        Connection con = null;
        try {
            con = DriverManager.getConnection("jdbc:sqlserver://" + ipAddress + ":" + service + ";databaseName=" + dbName, user, password);
        } catch (SQLException ex) {
            Logger.getLogger(ConexionSQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }

    public static void main(String[] args) {
        try {
            Connection con = ConexionSQL.obtenerConexion();
            System.out.println("Conexion efectuada...");
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
